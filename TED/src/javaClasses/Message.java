package javaClasses;

public class Message {
	
	public String from;
	public String content;
	public String to;
	
	public Message(String from, String content, String to){
		this.from = from;
		this.content = content;
		this.to = to;
	}
}
